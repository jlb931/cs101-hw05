
public class Test
{
	public static void main(String[] args)
	{
		Rectangle rect1 = new Rectangle();
		Rectangle rect2 = new Rectangle(5, 10);
		
		System.out.println("Rectangle 1 \nArea:" + rect1.getArea() + "\nPerimeter: " + rect1.getPerimeter());
		System.out.println();
		System.out.println("Rectangle 2 \nArea:" + rect2.getArea() + "\nPerimeter: " + rect2.getPerimeter());
	}
}
