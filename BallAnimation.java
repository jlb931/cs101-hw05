/**
 * Creates an array of the class Ball, sets the size and position of each, 
 */
import processing.core.*;

public class BallAnimation extends PApplet
{
	Ball[] balls = new Ball[10];
	public static void main(String[] args)
	{
		PApplet.main("BallAnimation");
	}
	
	public void settings()
	{
		size(1000, 1000);
	}
	
//	Creates array of balls with positions that vary by 50 pixels on x, and stay at 50 for y
	public void setup()
	{
		for(int i = 0; i<10; i++)
		{
			int pos = (i*100 + 50);
			balls[i] = new Ball(50, pos, 50);
		}
	}
	
	public void draw()
	{
		background(0);
//		Draws ellipses based on positions and sizes generated in setup, stored in the balls array.  Then uses drop function when mouse pressed to move balls down.
		for(int i =0; i<10; i++)
		{
			ellipse(balls[i].getXPosition(), balls[i].getYPosition(), balls[i].getSize(), balls[i].getSize());
			if(mousePressed)
			{
				balls[i].drop();
			}
		}
		
		
	}
}

